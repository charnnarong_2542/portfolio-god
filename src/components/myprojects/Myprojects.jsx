import React from "react";
import IMG3 from "../../assets/messageImage_1705846201533.jpg";
import IMG4 from "../../assets/messageImage_1705846221160.jpg";
import IMG2 from "../../assets/messageImage_1705846232301.jpg";
import IMG5 from "../../assets/messageImage_1705846249807.jpg";
import "./myprojects.css";

const Myprojects = () => {
  return (
    <section id="myprojects">
      <h5>My Recent Certificates</h5>
      <h2>Certificates</h2>

      <div className="container portfolio__container">
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG2} alt="" />
          </div>
          <h3>Certificate 1</h3>
          <small className="text-light">Aglie project management</small>
        </article>
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG4} alt="" />
          </div>
          <h3>Certificate 2</h3>
          <small className="text-light">Cybersucurity Awareness</small>
        </article>
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG3} alt="" />
          </div>
          <h3>Certificate 3</h3>
          <small className="text-light">Data driven Digital Government Transformation</small>
        </article>
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG5} alt="" />
          </div>
          <h3>Certificate 4</h3>
          <small className="text-light">Design thinking in Practice</small>
        </article>
      </div>
    </section>
  );
};

export default Myprojects;
