import React from "react";
import { BsBookmarkStar } from "react-icons/bs";
import { GiGraduateCap } from "react-icons/gi";
import { TfiWorld } from "react-icons/tfi";
import myImage from "../../assets/IMG_0533.jpg";
import "./about.css";

const About = () => {
  return (
    <section id="about">
      <h5>Get To Know</h5>
      <h2>About Me</h2>

      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={myImage} alt="Me" />
          </div>
        </div>

        <div className="about__content">
          <div className="about__cards">
            <article className="about__card">
              <GiGraduateCap className="about__icon" />
              <h5>Degree</h5>
              <small>
                B.E. Computer Engneering(UG) <br />
                <i>Chiang Mai University.</i>
              </small>
            </article>

            <article className="about__card">
              <BsBookmarkStar className="about__icon" />
              <h5>GPA</h5>
              <small>2.74</small>
            </article>

            <article className="about__card">
              <TfiWorld className="about__icon" />
              <h5>Domains</h5>
              <small>
                <ul>
                  <li>Net Centric</li>
                  <li>Mobile Applications</li>
                </ul>
              </small>
            </article>
          </div>
          <p>
            I'm a <b>Computer Engineering Undergradute</b> at the Chiang Mai University.I'm a new grad student from Chiang Mai University.I have been working at BAAC for 6 mounth.I'm very haappy with environment , friend and teammate. this work made for  Baac training course's personnel project.
          </p>
        </div>
      </div>
    </section>
  );
};

export default About;
